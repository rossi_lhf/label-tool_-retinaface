# -*-coding: utf-8 -*-

import cv2
import os


def saveImg(img, filePath):
    """
    save labed img to output folder, easy for user to check
    :param img:
    :param filePath:
    :return:
    """
    # print(filePath)
    tmpList = filePath.split("/")
    # outputFilePath = ""
    if len(tmpList) >= 2:
        outputFilePath = "./checkImages/" + tmpList[-2] + "_" + tmpList[-1]
    else:
        outputFilePath = "./checkImages/" + tmpList[-1]
    cv2.imwrite(outputFilePath, img)


def saveTxt(filePath, imgShape, rectList, xyList):
    """
    save label info to label.txt
    :param filePath:
    :param imgShape:
    :param rectList:
    :param xyList:
    :return:
    """
    f = "./label.txt"
    with open(f, "a") as file:
        tmpList = filePath.split("/")
        if len(tmpList) >= 2:
            labelFilePathStr = "# " + tmpList[-2] + "/" + tmpList[-1] + "\n"
        else:
            labelFilePathStr = "# " + tmpList[-1] + "\n"
        print("lableFilePathStr: ", labelFilePathStr)
        file.write(labelFilePathStr)

        hOriginal = imgShape[0]
        wOriginal = imgShape[1]

        print("xyList: ", xyList)
        print("rectList: ", rectList)
        for i in range(len(rectList)):
            rectx = rectList[i][0][0] if rectList[i][1][0] > rectList[i][0][0] else rectList[i][1][0]
            rectx = int((rectx - 560) * (wOriginal / 720))

            recty = rectList[i][0][1] if rectList[i][1][1] > rectList[i][0][1] else rectList[i][1][1]
            recty = int(recty * hOriginal / 480)

            rectw = abs(rectList[i][1][0] - rectList[i][0][0])
            rectw = int(rectw * wOriginal / 720)

            recth = abs(rectList[i][1][1] - rectList[i][0][1])
            recth = int(recth * hOriginal / 480)

            leyex = xyList[i * 5 + 0][0]
            leyex = (leyex - 560) * wOriginal / 720
            leyex = round(leyex, 3)

            leyey = xyList[i * 5 + 0][1]
            leyey = leyey * hOriginal / 480
            leyey = round(leyey, 3)

            reyex = xyList[i * 5 + 1][0]
            reyex = (reyex - 560) * wOriginal / 720
            reyex = round(reyex, 3)

            reyey = xyList[i * 5 + 1][1]
            reyey = reyey * hOriginal / 480
            reyey = round(reyey, 3)

            nosex = xyList[i * 5 + 2][0]
            nosex = (nosex - 560) * wOriginal / 720
            nosex = round(nosex, 3)

            nosey = xyList[i * 5 + 2][1]
            nosey = nosey * hOriginal / 480
            nosey = round(nosey, 3)

            lmoux = xyList[i * 5 + 3][0]
            lmoux = (lmoux - 560) * wOriginal / 720
            lmoux = round(lmoux, 3)

            lmouy = xyList[i * 5 + 3][1]
            lmouy = lmouy * hOriginal / 480
            lmouy = round(lmouy, 3)

            rmoux = xyList[i * 5 + 4][0]
            rmoux = (rmoux - 560) * wOriginal / 720
            rmoux = round(rmoux, 3)

            rmouy = xyList[i * 5 + 4][1]
            rmouy = rmouy * hOriginal / 480
            rmouy = round(rmouy, 3)

            infoStr = str(rectx) + " " + str(recty) + " " + str(rectw) + " " + str(recth) + " " + \
                      str(leyex) + " " + str(leyey) + " 0.0 " + \
                      str(reyex) + " " + str(reyey) + " 0.0 " + \
                      str(nosex) + " " + str(nosey) + " 0.0 " + \
                      str(lmoux) + " " + str(lmouy) + " 0.0 " + \
                      str(rmoux) + " " + str(rmouy) + " 0.0 " + "1.0" + "\n"
            print(infoStr)
            file.write(infoStr)

        file.close()


def lastLabeledImg():
    """
    find last labeled img file path
    :return:
    """
    lastPath = ""
    file = open("./label.txt", "r")
    line = file.readline()
    if len(line) > 0 and line[0] == "#":
        lastPath = line
    while line:
        line = file.readline()
        if len(line) > 0 and line[0] == "#":
            lastPath = line

    print("last file path: ", lastPath)
    if len(lastPath) == 0:
        return None
    tmplist = lastPath.split(" ")
    tmpstr = tmplist[-1]
    tmplist = tmpstr.split("/")

    if len(tmplist) == 2:
        subfolder = tmplist[0]
        imgfile = tmplist[1].strip("\n")
        return subfolder + "/" + imgfile
    else:
        subfolder = ""
        imgfile = tmplist[-1].strip("\n")
        return imgfile


if __name__ == "__main__":
    print("hoho")
    tmp = os.path.split("./images/folder1/WIN_20220303_12_09_22_Pro.jpg")
    print(tmp)
