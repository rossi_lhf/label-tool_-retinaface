# -*-coding: utf-8 -*-

import cv2
import numpy as np
import txtProc

winName = "label window"
cv2.namedWindow(winName, cv2.WINDOW_FREERATIO)
cv2.resizeWindow(winName, 640 * 2, 480)
cv2.moveWindow(winName, 0, 0)

img = np.zeros((480, 1280, 3), np.uint8)
imgClone = img.copy()
xyList = []
rectList = []

# 当鼠标按下时为True
drawing = False
# modeCount is used to calulate label status. 1 face rect and 5 face landmarks
modeCount = 0
ix, iy = -1, -1


# 创建回调函数
def draw_label(event, x, y, flags, param):
    global ix, iy, drawing, modeCount, xyList, rectList, imgClone
    if modeCount % 6 == 0:  # 1 face rect and 5 face landmarks
        # 当按下左键时返回起始位置坐标
        if event == cv2.EVENT_LBUTTONDOWN:
            drawing = True
            ix, iy = x, y
        # 当左键按下并移动时绘制图形，event可以查看移动，flag查看是否按下
        elif event == cv2.EVENT_MOUSEMOVE and flags == cv2.EVENT_FLAG_LBUTTON:
            if drawing:
                imgClone = img.copy()
                cv2.rectangle(imgClone, (ix, iy), (x, y), (0, 255, 0), 1)
                for i in range(len(rectList)):
                    cv2.rectangle(imgClone, rectList[i][0], rectList[i][1], (0, 255, 0), 1)
                for i in range(len(xyList)):
                    if i % 5 == 0:
                        cv2.circle(imgClone, xyList[i], 3, (0, 0, 255), -1)
                        cv2.putText(imgClone, "0", xyList[i], cv2.FONT_HERSHEY_SIMPLEX, 0.6, (0, 0, 255), 1)
                    if i % 5 == 1:
                        cv2.circle(imgClone, xyList[i], 3, (0, 0, 150), -1)
                        cv2.putText(imgClone, "1", xyList[i], cv2.FONT_HERSHEY_SIMPLEX, 0.6, (0, 0, 150), 1)
                    if i % 5 == 2:
                        cv2.circle(imgClone, xyList[i], 3, (0, 255, 0), -1)
                        cv2.putText(imgClone, "2", xyList[i], cv2.FONT_HERSHEY_SIMPLEX, 0.6, (0, 255, 0), 1)
                    if i % 5 == 3:
                        cv2.circle(imgClone, xyList[i], 3, (255, 0, 0), -1)
                        cv2.putText(imgClone, "3", xyList[i], cv2.FONT_HERSHEY_SIMPLEX, 0.6, (255, 0, 0), 1)
                    if i % 5 == 4:
                        cv2.circle(imgClone, xyList[i], 3, (150, 0, 0), -1)
                        cv2.putText(imgClone, "4", xyList[i], cv2.FONT_HERSHEY_SIMPLEX, 0.6, (150, 0, 0), 1)

        # 当鼠标松开时停止绘图
        elif event == cv2.EVENT_LBUTTONUP and drawing and abs(ix - x) > 10 and abs(iy - y) > 10:
            drawing = False
            rectList.append([(ix, iy), (x, y)])
            modeCount += 1
            print(modeCount)
    else:
        # 当按下左键时返回起始位置坐标
        if event == cv2.EVENT_LBUTTONDOWN:
            # drawing = True
            ix, iy = x, y
            xyList.append((x, y))
            # 绘制小圆点
            for i in range(len(xyList)):
                if i % 5 == 0:
                    cv2.circle(imgClone, xyList[i], 3, (0, 0, 255), -1)
                    cv2.putText(imgClone, "0", xyList[i], cv2.FONT_HERSHEY_SIMPLEX, 0.6, (0, 0, 255), 1)
                if i % 5 == 1:
                    cv2.circle(imgClone, xyList[i], 3, (0, 0, 150), -1)
                    cv2.putText(imgClone, "1", xyList[i], cv2.FONT_HERSHEY_SIMPLEX, 0.6, (0, 0, 150), 1)
                if i % 5 == 2:
                    cv2.circle(imgClone, xyList[i], 3, (0, 255, 0), -1)
                    cv2.putText(imgClone, "2", xyList[i], cv2.FONT_HERSHEY_SIMPLEX, 0.6, (0, 255, 0), 1)
                if i % 5 == 3:
                    cv2.circle(imgClone, xyList[i], 3, (255, 0, 0), -1)
                    cv2.putText(imgClone, "3", xyList[i], cv2.FONT_HERSHEY_SIMPLEX, 0.6, (255, 0, 0), 1)
                if i % 5 == 4:
                    cv2.circle(imgClone, xyList[i], 3, (150, 0, 0), -1)
                    cv2.putText(imgClone, "4", xyList[i], cv2.FONT_HERSHEY_SIMPLEX, 0.6, (150, 0, 0), 1)
            modeCount += 1
            print(modeCount)


def run(intputImg, filePath, imgShape):
    """
    下面把回调函数与OpenCV窗口绑定在一起，在主循环中奖'm'键与模式转换绑定在一起
    """
    global img, imgClone, xyList, rectList, modeCount
    img = intputImg
    imgClone = img.copy()

    cv2.setMouseCallback(winName, draw_label)
    while True:
        cv2.imshow(winName, imgClone)
        k = cv2.waitKey(1)

        if k == 13:  # enter
            txtProc.saveImg(imgClone, filePath)
            txtProc.saveTxt(filePath, imgShape, rectList, xyList)
            xyList = []
            rectList = []
            modeCount = 0
            return 1  # label ok

        if k == 27:  # esc
            xyList = []
            rectList = []
            modeCount = 0
            return 0  # cancel this pic's label

        if k == ord("q"):
            xyList = []
            rectList = []
            modeCount = 0
            return -1  # cancel this pic's label


if __name__ == "__main__":
    print("haha")
